package com.princess.sms.webapp.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.princess.sms.model.Language;
import com.princess.sms.model.LanguageList;
import com.princess.sms.service.LanguageService;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
public class LanguageControllerTest {
  private static final Logger LOGGER = LoggerFactory.getLogger(LanguageControllerTest.class);

  @InjectMocks
  private LanguageController languageController;

  private MockMvc mvc;

  @Mock
  private LanguageService languageService;


  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    mvc = MockMvcBuilders.standaloneSetup(languageController).build();
  }

  @Test
  public void testFindCurrentLanguage() throws Exception {
    List<Language> languages = new ArrayList<Language>();
    Language l = new Language();
    l.setCountryCode("US");
    l.setLanguageCode("EN");
    languages.add(l);
    LanguageList languageList = new LanguageList(languages);


    Mockito.when(languageService.findSupportedLanguages()).thenReturn(languageList);
    mvc.perform(MockMvcRequestBuilders.get("/languages/support").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andExpect(content().string(equalTo(
            "{\"count\":1,\"languages\":[{\"createdBy\":null,\"dateCreated\":null,\"dateModified\":null,\"modifiedBy\":null,\"countryCode\":\"US\",\"languageCode\":\"EN\",\"description\":null,\"supported\":false}]}")));

  }
}
