package com.princess.sms.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
public class Language extends AbstractBaseObject {

  // TODO : Write unit test for this
  /**
   * 
   */
  private static final long serialVersionUID = -1649976497480806982L;
  private String countryCode;
  private String languageCode;
  private String description;
  private boolean supported;

  /**
   * Method equals.
   * 
   * @param object Object
   * 
   * 
   * 
   * 
   * @return boolean
   */
  @Override
  public boolean equals(Object object) {
    if (!(object instanceof Language)) {
      return false;
    }
    Language rhs = (Language) object;
    return new EqualsBuilder().append(this.countryCode, rhs.countryCode).append(this.languageCode, rhs.languageCode)
        .isEquals();
  }

  /**
   * Method getCountryCode.
   * 
   * 
   * @return String
   */
  public String getCountryCode() {
    return countryCode;
  }

  /**
   * Method setCountryCode.
   * 
   * @param countryCode String
   */
  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  /**
   * Method getLanguageCode.
   * 
   * 
   * @return String
   */
  public String getLanguageCode() {
    return languageCode;
  }

  /**
   * Method setLanguageCode.
   * 
   * @param languageCode String
   */
  public void setLanguageCode(String languageCode) {
    this.languageCode = languageCode;
  }

  /**
   * Method getDescription.
   * 
   * 
   * @return String
   */
  public String getDescription() {
    return description;
  }

  /**
   * Method setDescription.
   * 
   * @param description String
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Method isSupported.
   * 
   * 
   * @return boolean
   */
  public boolean isSupported() {
    return supported;
  }

  /**
   * Method setSupported.
   * 
   * @param supported boolean
   */
  public void setSupported(boolean supported) {
    this.supported = supported;
  }

  /**
   * Method hashCode.
   * 
   * 
   * 
   * 
   * 
   * @return int
   */
  @Override
  public int hashCode() {
    return new HashCodeBuilder(1, 31).append(countryCode).append(languageCode).toHashCode();
  }

  /**
   * Method toString.
   * 
   * 
   * 
   * 
   * 
   * @return String
   */
  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("countryCode", this.countryCode)
        .append("languageCode", this.languageCode).toString();
  }
}
