package com.princess.sms.model;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.ObjectUtils;

/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * 
 * @version $Revision: 1.0 $
 */
public abstract class AbstractBaseObject
    implements Serializable {
  private static final long serialVersionUID = -7005794759447306717L;

  protected String createdBy;

  protected Date dateCreated;

  protected Date dateModified;

  protected String modifiedBy;

  /**
   * Method equals.
   * 
   * @param o Object
   * 
   * 
   * 
   * 
   * @return boolean
   */
  @Override
  public abstract boolean equals(Object o);

  /**
   * 
   * 
   * 
   * 
   * 
   * @return the createdBy
   */
  public String getCreatedBy() {
    return createdBy;
  }

  /**
   * 
   * 
   * 
   * 
   * 
   * @return the dateCreated
   */
  public Date getDateCreated() {
    return ObjectUtils.clone(dateCreated);
  }

  /**
   * 
   * 
   * 
   * 
   * 
   * @return the dateModified
   */
  public Date getDateModified() {
    return ObjectUtils.clone(dateModified);
  }

  /**
   * 
   * 
   * 
   * 
   * 
   * @return the modifiedBy
   */
  public String getModifiedBy() {
    return modifiedBy;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public abstract int hashCode();

  /**
   * @param createdBy the createdBy to set
   */
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  /**
   * @param dateCreated the dateCreated to set
   */
  public void setDateCreated(Date dateCreated) {
    this.dateCreated = ObjectUtils.clone(dateCreated);
  }

  /**
   * @param dateModified the dateModified to set
   */
  public void setDateModified(Date dateModified) {
    this.dateModified = ObjectUtils.clone(dateModified);
  }

  /**
   * @param modifiedBy the modifiedBy to set
   */
  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  /**
   * Method toString.
   * 
   * 
   * 
   * 
   * 
   * @return String
   */
  @Override
  public abstract String toString();

}
